## Übungsaufgaben zum Thema Backup-Erstellung

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/111912599159326948</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/backup-uebungsaufgaben</span>

> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Übungsaufgaben mit Lösungen zu Backups (z.B. zur Vorbereitung auf die [Teil-1-Prüfung der IT-Berufe](https://oer-informatik.de/fi-teil1-pruefung)). Welche Backupstrategien gibt es, welche Vor- und Nachteile haben Sie und welche Dateien sind für ein Restore nötig?._

#### Grundlagen Backup-Strategien

Man unterscheidet drei wesentliche Backup-Strategien. Nenne deren Namen und Merkmale!<button onclick="toggleAnswer('bkp1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bkp1">
 
- Inkrementelles Backup

- Differenzielles Backup

- Vollbackup

(Merkmale werden in der Antwort der nächsten Aufgabe genannt.)

</span>


#### Vor- und Nachteile von Backup-Strategien

Erläutere für jede dieser drei Backup-Strategien kurz die Vor- und Nachteile: <button onclick="toggleAnswer('bkp2')">Antwort ein/ausblenden</button>


|Name|Beschreibung|Vorteil|Nachteil|
|---|---|---|---|
|1. ||||
|2. ||||
|3. ||||

<span class="hidden-answer" id="bkp2">

|Name|Beschreibung|Vorteil|Nachteil|
|---|---|---|---|
|1. **Inkrementelles Backup**|Die Backups bauen aufeinander auf: gespeichert werden nur die Änderungen zum vorigen Backup.|Es wird weniger Speicherplatz benötigt, als bei einem differenziellen Backup.|Für eine komplette Wiederherstellung müssen viele Einzelbackups (alle Inkremente) einbezogen werden.|
|2. **Differenzielles Backup** |Es wird jeweils nur die Differenz der Daten zum letzten Vollbackup gespeichert|Für eine komplette Wiederherstellung müssen nur das Vollbackup und ein differenzielles Backup einbezogen werden.|Es wird mehr Speicherplatz benötigt als bei inkrementellen Backups.|
|3. **Voll-Backup**|Es werden alle Daten gesichert, unabhängig davon, ob sie auf bisherigen Backups bereits vorhanden sind.|Für eine komplette Wiederherstellung muss nur ein Backup einbezogen werden.|Es wird sehr viel Speicherplatz benötigt. Der Vorgang dauert sehr lange.|

</span>

#### Ursachen Datenverlust

Welche Ursachen für Datenverlust kann es geben, vor denen ein Backup schützt? <button onclick="toggleAnswer('bkp3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bkp3">

- Versehentliches Löschen

- bewusstes / böswilliges Löschen

- technische Defekte in der speichernden / schreibenden Hardware

- Diebstahl

- Brand, Wasser, mechanische Zerstörung

</span>

#### Grundlagen Restore von kombinierten Backups

Ein Skript erstellt Backups und benennt die Backup-Dateien nach folgendem Muster: 

    - Jeder Datei ist das Datum vorangestellt (Präfix nach dem Muster YY-MM-DD). 

    - Als zweiter Namensbestandteil werden Vollbackups mit fullbackup, inkrementelle mit incbackup und differenzielle mit diffbackup bezeichnet. 

    - Es handelt sich um eine gezippte (komprimierte) tar-Datei mit entsprechender Dateiendung 

Jeden Abend um 23 Uhr wird das Backupskript gestartet, es kann davon ausgegangen werden, dass es bis 23:45 beendet ist. 

    - Ist der Tag der erste Freitag im Monat, dann wird ein neues Vollbackup erzeugt.

    - Ist der Tag ein anderer Freitag, dann wird ein differenzielles Backup zum Vollbackup erstellt.

    - An allen anderen Tagen wird ein inkrementelles Backup zum Vortag erstellt.

![Kalender des 1. Quartals 2023 (1. Jan is So, 1. Feb ist Mi, 1. März is Mi)](backup_src/kalenderq1-23.png)

a) Listen Sie alle Backupdateien auf, die sie zur Wiederherstellung des Stands vom 2. April 2023 benötigen (einschließlich Backup dieses Tages). <button onclick="toggleAnswer('bkp3a')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bkp3a">

- 23-03-03 fullbackup.tgz

- 23-03-31 diffbackup.tgz 

- 23-04-01 inkbackup.tgz

- 23-04-02 inkbackup.tgz

</span>

b) Listen Sie alle Backupdateien auf, die sie zur Wiederherstellung der Dateien am 3. März 2023 benötigen (einschließlich Backup dieses Tages). <button onclick="toggleAnswer('bkp3b')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bkp3b">	

- 23-03-03 fullbackup.tgz

</span>

c) Listen Sie alle Backupdateien auf, die sie zur Wiederherstellung der Dateien am 16. Februar 2023  benötigen (einschließlich Backup dieses Tages) <button onclick="toggleAnswer('bkp3c')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="bkp3c">	

- 23-02-03 fullbackup.tgz

- 23-02-10 diffbackup.tgz 

- 23-02-11 inkbackup.tgz

- 23-02-12 inkbackup.tgz

- 23-02-13 inkbackup.tgz

- 23-02-14 inkbackup.tgz

- 23-02-15 inkbackup.tgz

- 23-02-16 inkbackup.tgz

</span>
