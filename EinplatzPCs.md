# Einplatz-PCs

## Arbeitsplatzrechner-Arten

Nennen Sie die charakteristischen Eigenschaften sowie Vorteile und Nachteile von:

- Thin Clients
- Convertibles
- Netbooks
- All-in-one Geräte

## Komponenten

Nennen Sie die Vorteile von USB-3.1 ggü. USB-2!

Nennen Sie die Vor- und Nachteile von SSD gegenüber HDD!








## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/](). Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (Hannes Stein, OER Informatik) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
