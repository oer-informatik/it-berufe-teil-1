# ***CHANGE NAME HERE***

* [***CHANGE NAME HERE***](https://oer-informatik.gitlab.io/***CHANGE NAME HERE***.html) [(als PDF)](https://oer-informatik.gitlab.io/***CHANGE NAME HERE***.pdf)


# Infos zur genutzten gitlab CI und deren Nachnutzung:
Die HTML und PDF-Dateien wurden mit Hilfe der Konfiguraiton der [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab erstellt.
Die Vorlage findet sich hier: [https://gitlab.com/TIBHannover/oer/course-metadata-test/](https://gitlab.com/TIBHannover/oer/course-metadata-test/).
