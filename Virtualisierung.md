# Virtualisierung

## Virtuelle Maschinen

Welche Vor- und Nachteile bietet die Virtualisierung im Vergleich zu physischen Servern?

|Vorteile Virtualisierung von Servern|Vorteile physische Server|
|---|---|
|Flexibilität,<br/>bessere Skalierbarkeit,<br/>bessere Portabilität,<br/>geringere Wartungskosten,<br/>vereinfachte Bereitstellung und Wartung, <br/>vereinfachte Sicherung,|keine Performanceprobleme durch ungeteilte Hardware,<br/>keine zusätzliche Abstraktionsschicht (Virtualisierung),<br/>|

## Virtualisierungs-Arten

### Erläutern Sie die folgenden Begriffe:

**Speicher-Virtualisierung**: <span class="hidden-text">Physisch vorhandener Speicherplatz (Arbeitsspeicher oder Volumes/Laufwerke) werden bedarfsgerecht unterschiedlichen VM-Clients, Containern oder Programmen zugeteilt.</span>

**Prozessorvirtualisierung**:
<span class="hidden-text">Physisch vorhandene Rechenkerne (CPU) werden bedarfsgerecht unterschiedlichen VM-Clients, Containern oder Programmen zugeteilt.</span>

Welche weiteren Komponenten eines Servers konnen virualisiert werden?
<span class="hidden-text">
- Grafikkarte
- Netzwerkkarte
<span>

## Vorteile _On Premises_ gegenüber Cloud

Welche Vorteile bestehen bei dem Betrieb einer Software auf eigenen Servern (direkt vor Ort), also _on premises?

Welche Vorteile bestehen bei dem Betrieb einer Software bei Hosting durch einen Provider?

Was versteht man unter dem Begriff _Software as a service_?


## Links und weitere Informationen
