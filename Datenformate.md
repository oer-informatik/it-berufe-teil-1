# Datenformate

## Erläutern Sie die charakteristischen Eigenschaften des Datenformats CSV und geben sie ein kurzes Beispiel (mind. 3 Zeilen mit je 3 Werten)

**CSV**: <button onclick="toggleAnswer('datenformat1')">Antwort ein/ausblenden</button>

<span class="hidden-text" id="datenformat1">

CSV steht für "Comma-separated Values": Text wird in Segmente geteilt, die durch ein Trennzeichen voneinander separiert werden. Auch wenn der Name von Komma-getrennten Werten spricht sind dies häufig andere Trennzeichen. Üblich ist im deutschsprachigen Raum insbesondere der Semikolon ("Strichpunkt").

Ein Beispiel:
```csv
ID;Vorname;Nachname;Haarfarbe
1;Ringo;Starr;grau
2;George;Harrison;grau (zuletzt)
3;Paul;McCartney;grau
4;John;Lennon;grau
```

</span>

## Erläutern Sie die charakteristischen Eigenschaften des Datenformats XML

**XML**: <button onclick="toggleAnswer('datenformat2')">Antwort ein/ausblenden</button>

<span class="hidden-text" id="datenformat2">

Steht für "Extensible Markup Language": Hierarchisch strukturierte Auszeichnungssprache. Informationen werden in Tags, Attributen und zwischen Tags notiert. XML-Dateien können syntaktisch korrekt ("wohlgeformt") und einer Grammatik folgend semantisch korrekt ("valide" gegen ein Schema) sein.

</span>

## Erläutern Sie die charakteristischen Eigenschaften des Datenformats JSON

<button onclick="toggleAnswer('datenformat3')">Antwort ein/ausblenden</button>

<span class="hidden-text" id="datenformat3">

</span>

## Erläutern Sie die charakteristischen Eigenschaften des Datenformats YAML


<button onclick="toggleAnswer('datenformat4')">Antwort ein/ausblenden</button>

<span class="hidden-text" id="datenformat4">

</span>
