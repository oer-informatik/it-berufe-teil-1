## Datensicherheit

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/datensicherheit-uebungsaufgaben</span>

> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Übungsaufgaben mit Lösungen zur Datensicherheit und Verschlüsselung (z.B. zur Vorbereitung auf die [Teil-1-Prüfung der IT-Berufe](https://oer-informatik.de/fi-teil1-pruefung))..._

## Einfache Verschlüsselung mit logischen Verknüpfungen

Das Wort "unknackbar" soll mit dem symmetrischen Schlüssel "Safe" verschlüsselt werden.

|Geheimnis|u|n|k|n|a|c|k|b|a|r|
|---|---|---|---|---|---|---|---|---|---|---|
|Schlüssel (Key)|S|a|f|e|S|a|f|e|S|a|

Es trifft also der erste zu veschlüsselnde Buchstabe `u` (ASCII-Code 117 (x75<sub>16</sub>): 0111 0101) auf die Schlüsselsequenz `S` (ASCII-Code 83 (x53<sub>16</sub>): 0101 0011).


Die Verschlüsselung soll durch bitweise logische Verknüpfung erzeugt werden. Füllen Sie für den beispielhaften Ausgangswert und Schlüssel die Tabelle mit den Ergebnissen der logischen Verknüpfung aus!

|Ausgangswert<br/>`A`|Key<br/>`K`|`A AND K`|`A OR K`|`A NAND K`|`A NOR K`|`A XOR K`|`NOT A`|
|---|---|---|---|---|---|---|---|
|||![AND-Gatter](images/AND.PNG)|![OR-Gatter](images/OR.PNG)|![Logik-Gatter](images/Logikgatter.PNG)|![Logik-Gatter](images/Logikgatter.PNG)|![Logik-Gatter](images/Logikgatter.PNG)||
|0|0|<span class="hidden-text">0</span>|<span class="hidden-text">0</span>|__|__|__|__|
|1|1|<span class="hidden-text">1</span>|<span class="hidden-text">1</span>|__|__|__|__|
|0|1|<span class="hidden-text">0</span>|<span class="hidden-text">1</span>|__|__|__|__|
|1|1|<span class="hidden-text">1</span>|<span class="hidden-text">1</span>|__|__|__|__|
|0|0|<span class="hidden-text">0</span>|<span class="hidden-text">0</span>|__|__|__|__|
|1|0|<span class="hidden-text">0</span>|<span class="hidden-text">1</span>|__|__|__|__|
|1|1|<span class="hidden-text">1</span>|<span class="hidden-text">1</span>|__|__|__|__|
|1|1|<span class="hidden-text">1</span>|<span class="hidden-text">1</span>|__|__|__|__|


Begründen Sie anhand der obigen Liste, welche logische Verknüpfung sich zum Verschlüsseln eignet!

Grenzen Sie die Begriffe Datenschutz und Datensicherheit gegeneinander ab!

**Datenschutz**: <span class="hidden-text">Daten von  identifizierten oder identifizierbaren Personen (personenbezogenen Daten) werden durch die Regelungen der [Datenschutz-Grundverordnung (DS-GVO)](https://www.bmj.de/DE/Themen/FokusThemen/DSGVO/_documents/Amtsblatt_EU_DSGVO.pdf;jsessionid=07CEA980A4E27B2C4B9313AF2A5C8EED.2_cid324?__blob=publicationFile&v=1) und des [Bundesdatenschutzgesetz (BDSG)](https://www.gesetze-im-internet.de/bdsg_2018/inhalts_bersicht.html) geregelt. Im Kern des Datenschutzes steht das  Recht auf informationelle Selbstbestimmung.</span>

**Datensicherheit:** <span class="hidden-text">Unter dem Begriff "Datensicherheit" werden alle Maßnahmen zusammengefasst, die Unbefugte daran hindern Daten zu erstellen, lesen, ändern oder löschen.</span>


## Zwei Faktor Authentifizierung (2FA)

Erläutern Sie das Prinzip der zwei Faktor Authentifizierung:
<span class="hidden-text">Die Authentifizierung erfolgt über zwei unterschiedliche Faktoren, im Idealfall über einen Besitz (z.B. Bank-Karte) und ein geteiltes Geheimnis (z.B. PIN). Die Verteilung auf unterschiedliche Faktoren erhöht die Sicherheit gegenüber Ein-Faktor verfahren (z.B. nur Passwort oder nur Schlüssel).</span>

Welche unterschiedlichen Faktoren sind möglich?
<span class="hidden-text">
Geheimnisse:
- Passwort
- Pin
- Transaktionsnummer

Besitz:
- Chipkarte
- Handy mit Token (ein Token selbst ist eher ein Geheimnis)
- Schlüssel

Biometrische Kennzeichen (eigentlich auch "Besitz"):
- Fingerabdruck
- Irisscan
- Gesichtserkennung
</span>

## Gefahren

Welche Gefahren birgt:

- das Öffnen von E-Mail-Anhängen

- Distributed Denial of Service

## Zuordnung der Gefahren

Über ein verteiltes Netz wird versucht, einen Rechner zu überlasten, um ihn aus dem Netz zu nehmen (und so ggf. Schadcode einzuschmuggeln).
	

DDoS

Beeinflussungen von Menschen mit dem Ziel, bei Personen bestimmte Verhaltensweisen hervorzurufen, sie zum Beispiel zur Preisgabe von vertraulichen Informationen oder zum öffnen nicht vertrauenswürdiger Dateien zu bewegen.
	

Social Engineering

Verschleiern der eigenen Identiät, um Authentifizierungs- und Identifikationsverfahren zu untergraben
	

Spoofing

Versuch, persönliche Daten abzuschöpfen z.B. über gefälschte Websites
	

Phishing

Ein Programm, dass vermeintlich einen anderen Zweck hat, installiert Schadsoftware.
	

Trojaner

Erpressungssoftware, die das bestehende System verschlüsselt
	

Ransomware


