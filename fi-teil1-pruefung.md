## Teil-1-Prüfung der IT-Berufe

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/111909599448887967</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/fi-teil1-pruefung</span>

> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Der Artikel gibt eine subjektive (nicht abschließende) Übersicht der wesentlichen Themen für die Teil 1-Prüfung der IT-Berufe - inklusive der aktuellen Änderungen im Prüfungskatalog. In einigen Fällen gibt es Links zu Übungsaufgaben und Infotexten oder Hinweise auf Materialien. Ich werde den Artikel in den nächsten Wochen beständig um Infos und Übungsaufgaben erweitern._

### Rahmendaten Teil-1-Prüfung

Die Teil-1-Prüfung findet regulär nach 18 Monaten Ausbildung statt und wird jeweils im Frühjahr (oft Ende Februar) und Herbst (oft Ende September, v.a. für Verkürzer*innen) angeboten. Die [Fachinformatikerausbildungsverordnung](https://www.gesetze-im-internet.de/fiausbv/BJNR025000020.html) gibt in §9(2) vor, dass Prüflinge in der Lage sein müssen...


> 1. Kundenbedarfe zielgruppengerecht zu ermitteln,
> 2. Hard- und Software auszuwählen und ihre Beschaffung einzuleiten,
> 3. einen IT-Arbeitsplatz zu konfigurieren und zu testen und dabei die Bestimmungen und die betrieblichen Vorgaben zum Datenschutz, zur IT-Sicherheit und zur Qualitätssicherung einzuhalten,
> 4. Kunden und Kundinnen in die Nutzung des Arbeitsplatzes einzuweisen und
> 5. die Leistungserbringung zu kontrollieren und zu protokollieren.

Der Beschaffungsprozess eines Einplatz-PC steht also im Zentrum und bildet den Schwerpunkt. Leider haben die Fragen der bisherigen Teil-1-Prüfungen erkennen lassen, dass das Thema nicht in vorhersagbarer Breite, sondern mit deutlich ausgeprägten wechselnden Schwerpunkten abgefragt wurde. Es ist noch keine wiederkehrende Struktur bei der Themenauswahl erkennbar, wie wir sie etwa nach alter Prüfungsordnung bei der Zwischenprüfung oder in der _ganzheitlichen Aufgabe 2_ (GA2) hatten. 

Das ist insbesondere deshalb für die Vorbereitung relevant, als in der Prüfung nur noch offene Fragen vorkommen (anders als der Multiple-Choice-Test der Zwischenprüfung) und jeder Handlungsschritt bearbeitet werden muss (keine Abwahlmöglichkeit eines Handlungsschritts wie bei der _ganzheitlichen Aufgabe 2_). Zudem umfasst der Prüfungskatalog eine beachtliche Breite an Themen, die alle in der ersten Hälfte der Ausbildung maximal kurz angeschnitten werden können.

Die Prüfung fließt mit **20% in die Abschlussnote** ein - sie ist damit die wichtigste schriftliche Einzelprüfung. Auch sehr schlechte Ergebnisse in der Teil-1-Prüfung bleiben bis zum Schluss bestehen und verschlechtern so die Abschlussnote oder verhindern evtl. sogar den Berufsabschluss. Kurzum: **es lohnt sich, sich gut vorzubereiten**!

### Materialien zur Vorbereitung auf die Teil-1-Prüfung

Mit welchen Materialien kann man sich am besten auf die Prüfung vorbereiten?

Auch wenn die Prüfung zum betrieblichen Teil der dualen Ausbildung gehört, sollten die allermeisten Themen im Rahmen des Berufsschulunterrichts des ersten Jahres (Lernfeld 1-5) bzw. zu Beginn des zweiten Jahres (Lernfeld 6, ggf. Lernfeld 8) behandelt worden sein. Daher stellt das **Fachbuch** und in den anderen schulischen Unterlagen eine gute Basis dar. Wer rechtzeitig genug anfängt, sollte ruhig das Buch bis zur Teil-1-Prüfung komplett gelesen haben, für alle anderen ist mindestens das Inhaltsverzeichnis Pflichtlektüre, um aus der Themenbreite offene Punkte herausarbeiten zu können. [Lehrbuch des Westermannverlags für das 1. Ausbildungsjahr](https://www.westermann.de/artikel/978-3-14-220000-2/IT-Berufe-Grundstufe-Lernfelder-1-5) sowie, wer es braucht das [zugehörige Arbeitsbuch](https://www.westermann.de/artikel/978-3-14-220000-2/IT-Berufe-Grundstufe-Lernfelder-1-5) (letzteres ist aus meiner Sicht viel Papier und wenig Nutzen). Wer ausgestorbene Innenstädte nicht mag und bis zwei zählen kann bestellt diese Bücher natürlich im lokalen Buchhandel!

Stichwort Themenbreite: der jeweilige Prüfungserstellungsausschuss veröffentlicht für jeden IT-Beruf einen Prüfungskatalog, aus dem die möglichen Themen der Teil-1 und Teil-2-Prüfung hervorgehen. Dieser kann [über den UForm-Verlag](https://www.u-form-shop.de/abschlusspruefung/fachinformatiker-fachinformatikerin/anwendungsentwicklung/fachinformatiker-fachinformatikerin-anwendungsentwicklung-pruefungskatalog-fuer-die-ihk-abschlusspruefung) erworben werden (lokaler Buchhandel). Der Prüfungskatalog ist 2024 geringfügig angepasst worden - also achte bitte darauf, dass Du über den aktuellen Stand verfügst! Es ist aber eine reine Aneinanderreihung von allgemeinen Stichwörtern ohne weitere Erläuterungen - aus meiner Sicht ist hier das Inhaltsverzeichnis des Fachbuchs ähnlich hilfreich. 

Damit wäre es an der Zeit, sich die offenen Punkte und die verfügbare Zeit in einem Lernplaner zu organisieren. Aus Versehen lernt man dabei direkt die S.M.A.R.T-Technik (Prüfungsrelevant), in dem man seine Lernziele **s**pezifisch, **m**essbar, erreichbar (**a**chievable), angemessen (**r**easonable) und **t**erminiert formuliert:

|Themengebiet|Lernbedarf<br/>(0: kann ich<br/>10: viel lernen)|Relevanz<br/>(0: Randthema<br/> 10: häufig abgefragt)|übe ich am….|für … min|Quellen<br/>Übungsaufgaben<br/>genutzte Unterlagen|Offene Fragen<br/>(ggf. neuen Task erstellen)|erledigt am|
|---|---|---|---|---|---|---|---|
|S.M.A.R.T.|0|8|10.2.|15min|[S.M.A.R.T.](https://de.wikipedia.org/wiki/SMART_(Projektmanagement))||10.2.|

Ich trage immer zuerst offene Themen, Lernbedarf und Relevanz ein und im zweiten Schritt für alle mit großer Relevanz und großem Lernbedarf Termine. Häufig bleiben die restlichen Spalten leer - bis auf das "erledigt"-Datum, das ist aber nicht schlimm - schließlich wollen wir nicht die gesamte Zeit zur Verwaltung nutzen. Bei der Planung und Organisation hilft auch ein halb ausgefüllter Lernplaner!

Bleibt das Elend mit den Übungsaufgaben. Leider werden alte Prüfungsaufgaben nach wie vor nur über den [U-Form-Verlag](https://www.u-form-shop.de/abschlusspruefung/fachinformatiker-fachinformatikerin-1) vertrieben, eine Veröffentlichung wie in diesem Artikel von [Netzpolitik.org](https://netzpolitik.org/2015/klausurheberrecht-alte-abituraufgaben-duerfen-nicht-veroeffentlicht-werden) ist mir bisher nicht bekannt. Gibt es hierzu neue Initiativen? Ich bin für jeden Hinweis dankbar!

Wem die Prüfung vorliegt, der sollte auf jeden Fall die bisherigen Prüfungen durcharbeiten. Zum Einstieg kann man ein/zwei **alte Zwischenprüfungen** (vor 2021) und deren Multiple-Choice-Fragen heranziehen (Vorsicht: diese sind viel leichter und erwartbarer als die Teil-1-Prüfung!).

Hilfreicher sind die offenen Fragen des IT-berufsübergreifenden Teils der alten Abschlussprüfung: die **Ganzheitliche Aufgabe 2 (GA2)**. Wem hier alte Prüfungen vorliegen (z.B. aus dem Zeitraum 2017-2021), der hat schon ein ordentliches Paket an relativ relevanten Übungsaufgaben, kann jedoch einige Themenbereiche getrost überspringen (s.u.)!

Schließlich sollte man auf jeden Fall die **bisherigen Teil-1-Aufgaben** durchgehen. Diese gibt es seit Herbst 2021 im Halbjahrestakt - Stand heute also fünf Stück.

Sehr empfehlen kann ich den [KIT-Prüfungsvorbereitungskurs von Herr NM](https://herr-nm.github.io/MMBbS_KIT_PVAP1/): sehr viele umfassende Aufgaben, super zugeschnitten auf die Themen der Teil-1-Prüfung!

Gibt es weitere gute Übersichten / Aufgabensammlungen? Gerne kurze Nachricht an mich!

Ein paar Übungsaufgaben aus alten Klassenarbeiten werde ich peu-à-peu auch versuchen, hier zu veröffentlichen. Jetzt wollen wir aber zunächst mal schauen, was alles abgefragt werden kann 

### Themen der Teil-1-Prüfung

Thematisch schreibt die [Fachinformatikerausbildungsverordnung](https://www.gesetze-im-internet.de/fiausbv/BJNR025000020.html) in §4(2) sieben Bereiche vor, aus denen der Prüfungserstellungsausschuss den Prüfungskatalog^[Der Prüfungskatalog selbst ist eine relativ offene Aneinanderreihung von Oberthemen und hilft nur bedingt bei der Vorbereitung - trotzdem kann es ganz interessant sein, sich mal die Themenauswahl anzuschauen. Er kann [hier beim UForm-Verlag](https://www.u-form-shop.de/abschlusspruefung/fachinformatiker-fachinformatikerin/anwendungsentwicklung/fachinformatiker-fachinformatikerin-anwendungsentwicklung-pruefungskatalog-fuer-die-ihk-abschlusspruefung) erworben werden.] erarbeitet hat: 

![Mindmap zur Teil1-Prüfung mit den unten genannten sieben Oberpunkten und den jeweils im folgenden  Abschnitt genannten Unterpunkten](images/teil1mindmap.svg)

>    1. Planen, Vorbereiten und Durchführen von Arbeitsaufgaben in Abstimmung mit den kundenspezifischen Geschäfts- und Leistungsprozessen, 
>    2. Informieren und Beraten von Kunden und Kundinnen, 
>    3. Beurteilen marktgängiger IT-Systeme und kundenspezifischer Lösungen, 
>    4. Entwickeln, Erstellen und Betreuen von IT-Lösungen, 
>    5. Durchführen und Dokumentieren von qualitätssichernden Maßnahmen, 
>    6. Umsetzen, Integrieren und Prüfen von Maßnahmen zur IT-Sicherheit und zum Datenschutz, 
>    7. Erbringen der Leistungen und Auftragsabschluss

Ich will im Folgenden kurz zu den Bereichen Themen nennen, die konkret dazu gefragt werden können (das folgende ist keine abschließende Liste - die liefert der oben genannte Prüfungskatalog).

#### 1. Planen, Vorbereiten und Durchführen von Arbeitsaufgaben in Abstimmung mit den kundenspezifischen Geschäfts- und Leistungsprozessen

Das Projekt-Lernfeld ist erst im dritten Ausbildungsjahr angelegt, trotzdem werden zur Teil-1-Prüfung schon die Grundlagen des Projektmanagements abgefragt:

- [Merkmale eines Projekts](https://de.wikipedia.org/wiki/Projektmerkmal) sollten bekannt sein, das [_magische Dreieck_](https://de.wikipedia.org/wiki/Projektmanagement#Steuerungsgr%C3%B6%C3%9Fen) des Projektmanagements auch.

- Aufgaben zu [Netzplan](https://oer-informatik.de/netzplan) und Gantt finden sich zur Genüge in alten _ganzheitlichen Aufgaben 2_. Welche Unterschiede gibt es zwischen Netzplan](https://oer-informatik.de/netzplan)  und Gantt? Kritische Pfade und Pufferzeiten sollten erklärt, Termine und Pufferzeiten erklärt werden können.

- Zu Vorgehensmodellen sollten die Grundzüge klassischen dokumentengetriebenen Vorgehens (Wasserfall, V-Modell) gegenüber agilen Vorgehensmodellen abgegrenzt werden können. Was heißt iterativ, was inkrementell? Als agiles Vorgehensmodell wird am ehesten zu Scrum gefragt, ggf. noch Kanban/Lean in Grundzügen.

- Häufig wurde nach Teambildungs-Phasen (nach [Bruce Tuckman](https://de.wikipedia.org/wiki/Teambildung#Phasenmodell_nach_Tuckman_und_Klotz): Forming, Storming, Norming, Performing, Adjourning) gefragt - die zugehörigen Grafiken sollte man vor Augen haben!

- Es wird vorausgesetzt, dass man die Machbarkeit eines Projekts beurteilen kann: Machbarkeits- und Risikoanalyse sollten bekannte Begriffe sein, ebenso Vor- und Nachkalkulation.

- Den Begriff _[Stakeholder](https://oer-informatik.de/anforderungsanalyse-ermittlung#stakeholdermanagement)_ sollte man definieren können, ggf. auch Stakeholder über eine [Umfeldanalyse](https://oer-informatik.de/anforderungsanalyse-ermittlung#umfeldanalyse) ermitteln können.

- Die Aufgaben, Unterschiede und Autoren von [Lastenheft](https://oer-informatik.de/wasserfall_mcu_projekt_lastenheft) und [Pflichtenheft](https://oer-informatik.de/wasserfall_mcu_projekt_pflichtenheft) sollten bekannt sein.

- Die Ebenen für Supportanfragen und deren Aufgabe sollten bekannt sein (First-, Second- Third-Level Support)

Eine gute Aufgabensammlung findet sich im Blog [von Herr NM](https://herr-nm.github.io/MMBbS_KIT_PVAP1/01_projektmanagement.html)

#### 2. Informieren und Beraten von Kunden und Kundinnen

- Gerne gefragt wird nach Kommunikationsmodellen: Die Sender-Empfänger-Problematik bei der Kommunikation, das [Vier-Ohren-Modell](https://de.wikipedia.org/wiki/Vier-Seiten-Modell) von Schulz von Thun, das [Eisbergmodell](https://de.wikipedia.org/wiki/Eisbergmodell) und das zugehörige Pareto-Prinzip.

- Die unterschiedlichen Marktformen sollten bekannt sein (Monopol, Oligopol, Polypol...).

- Definition oder Durchführung einer [SWOT-Analyse](https://de.wikipedia.org/wiki/SWOT-Analyse) (Stärken, Schwächen, Chancen, Risiken)

- Beschaffungskalkulationen anhand gegebener Daten sollte durchgeführt werden können

- Angebotsvergleiche und Angebotsbewertungen wurden häufig in der GA2 abgefragt, Break-Even-Point-Analysis sollte durchgeführt werden können

- **Nutzwertanalysen** kamen häufig vor. 

- Unterschiedliche Kostenarten sollten bekannt sein (z.B. Unterscheidung variable und fixe Kosten)

#### 3. Beurteilen marktgängiger IT-Systeme und kundenspezifischer Lösungen

- Hier sollten die wesentlichen aktuellen PC-Komponenten und Techniken bekannt sein. Im Grunde kann man sich hier nur über regelmäßiges Lesen aktueller Artikel auf dem Stand halten (z.B. die Zeitschrift [c't](https://www.heise.de/ct) oder Tech-News wie [golem](https://www.golem.de/).

- PC-Komponenten sollten hinsichtlich technischer, wirtschaftlicher,ergonomischer und nachhaltiger Kriterien (z.B. Effizienz, Ressourcenverbrauch, Barrierefreiheit) beurteilt werden.

- Welche Arten IT-Devices gibt es und was zeichnet sie aus? (Desktop, Convertibles, Thin Clients, Tablets, Notebooks, Smartphones, All-in-One)

- Berechnungen von Leistung, Spannung, Stromstärke, Arbeit und Wirkungsgrad eines PC-Systems sowie den resultierenden Stromkosten

- Berechnungen von Datenaufkommen, Datenübertragungsraten und -zeiten

- Aufgaben der Netzwerkdevices (Router, Switch, Accesspoint) sollten ebenso bekannt sein wie aktuelle Ethernet- und WLAN-Standards. Auch Glasfaser (LWL) ist explizit genannt.

- Unterscheidung der Netzwerkkomponenten wie Router, Switch, Accesspoint werden erwartet

- Die wesentlichen Netzwerkprotokolle sollten den Schichten des OSI-Modells zugeordnet werden können. Der Prüfungskatalog nennt hier die Netzwerkprotokolle IP, TCP und UDP, die Datenübertragungsprotokolle HTTP und HTTPS, die Datei-Freigabeprotokolle SMB und NFS, die Mail-Protokolle SMTP und IMAP, das verschlüsselte Kommunikations- und VPN-Protokoll IPSEC, das verschlüsselte Datenübertragungsprotokoll SSH, das Konfigurationsprotokoll DHCP, das Hardwareadressen-Zuordnungsprotokoll ARP, das Verschlüsselungsprotokoll TLS und das Namensauflösungs-Protokoll DNS.

- WLAN-Zugangsmethoden mit PreSharedKey und Enterprise-Authentifizierung sollten bekannt sein

- Auch Dateisysteme stehen im Prüfungskatalog: Was ist FAT32, NTFS, APFS oder EXT4?

- Unterscheidung zwischen NAS, SAN und DAS (RAID ist nicht mehr Bestandteil der Teil 1 Prüfung)

- In diesen Themenkomplex fallen auch Virtualisierungstechniken: Hypervisor (Typ1/Typ2) und Container

- Einfache Adressierung in IPv4 und IPv6-Netzen (inkl. Interpretation von LinkLocal-Adressen usw.)

- Die Wirtschaftlichkeit sollte über unterschiedliche Kosten berechnet werden (Anschaffung / Betrieb, variable/fix, Lizenzen, Finanzierung)

- Unterschiedliche Modelle sollten verglichen werden können (Kauf, Leasing, Pay per Use)


#### 4. Entwickeln, Erstellen und Betreuen von IT-Lösungen

- Softwareprodukte sollten in unterschiedliche Kategorien eingeordnet werden können, denkbare Gruppen sind:

  - Anwendungssoftware / Betriebssystem / Firmware / BIOS

  - Standardsoftware / Branchensoftware / Individualsoftware

  - on premises / SaaS
  
- Bestandteile einer _Integrierten Entwicklungsumgebung_ (IDE)

- Lizenzmodelle: Was heißt OpenSource/GNU (Free as in free beer? / Free as in freedom?), Proprietäre Software, OEM, EULA, SaaS?

- [Pseudocode](https://oer-informatik.de/pseudocode) (Hier eine [Übersicht](https://oer-informatik.de/programmentwurfstechniken)) (Hinweis: Programmablaufplan (PAP) und Struktogramm sind nichtmehr Bestandteil der Teil 1-Prüfung)

- Unterscheidung zwischen Prozeduraler und objektorientierter Programmierung

- Grundlagen der Programmierung (Kontrollstrukturen, Datenstrukturen)

- Modellierung von objektorientierter Software mit dem [UML-Klassendiagramm ](https://oer-informatik.de/uml-klassendiagramm): inkl. Sichtbarkeit 

- Modellierung von Anwendungsfällen mit dem [UML-Use-Case-Diagramm](https://oer-informatik.de/uml-usecase)

- Modellierung von Abläufen mit dem [UML-Aktivitäts-Diagramm](https://oer-informatik.de/uml-aktivitaet)

- Unterscheidung zwischen Bibliotheken und Frameworks

- Unterscheidung von Skript- Interpreter- und Bytecodesprachen - entsprechend unterschied zwischen Compiler und Interpreter (und auch Linker steht noch im Katalog!)

- Interpretieren eines Programmcodes (auch in einer unbekannten Programmiersprache)

- Datenbankmodellierung (einfache [ER-Diagramme](https://oer-informatik.de/erm)) (Hinweis: Datenbankabfragen in einer Tabelle mit SQL ist nicht mehr Bestandteil der Teil 1 Prüfung)

- Einige Konsolenbefehle werden als bekannt vorausgesetzt - namentlich werden Befehle der Bash oder DOS-Eingabeaufforderung genannt:

  - Befehls-/Datei-/Verzeichnisoperationen `dir`, `del`, `copy` / `ls`, `mkdir`, `cp`, `chmod`, `alias`

  - Netzwerk-Debugging: `ping`, `ipconfig`, `ip`, `arp`, `traceroute`, `nslookup`, `iproute2`

#### 5. Durchführen und Dokumentieren von qualitätssichernden Maßnahmen

- [Sieben Grundsätze](https://de.wikipedia.org/wiki/Qualit%C3%A4tsmanagementnorm#Inhalte_der_ISO-9000-Normenreihe) des Qualitätsmanagements (ISO 9000)

- Management-Zyklus / Demingkreis: [Plan-Do-Check-Act (PDCA)](https://de.wikipedia.org/wiki/Demingkreis)

- Mess- und überprüfbare Zieldefinitionen mit [S.M.A.R.T.](https://de.wikipedia.org/wiki/SMART_(Projektmanagement))

- Software-Qualitätskriterien (z.B. nach [FURPS](https://oer-informatik.de/anforderungsanalyse#welche-arten-von-anforderungen-gibt-es), [ISO 9126](https://de.wikipedia.org/wiki/ISO/IEC_9126#/media/Datei:ISO_9126_quality_(de).svg))

- Fehler in Programmquellcode finden, Schreibtischtest

- Testprotokolle erstellen

#### 6. Umsetzen, Integrieren und Prüfen von Maßnahmen zur IT-Sicherheit und zum Datenschutz

- Begriffstrennung zwischen Datensicherheit und Datenschutz

- IT-Grundschutz ([Link: IT-Grundschutz-Übungsaufgaben](https://oer-informatik.de/it-grundschutz-uebungsaufgaben))

  - Schutzbedarfsanalyse

  - Definieren der Schutzziele Integrität, Vertraulichkeit und Verfügbarkeit

  - Technisch-Organisatorische Maßnahmen

- IT-Sicherheit

  - Unterschied zwischen Zugangs/Zutritts- und Zugriffsschutz

  - Backup ([Link: Backup-Übungsaufgaben](https://oer-informatik.de/backup-uebungsaufgaben))
  
  - Firewall
  
  - Verschlüsselung (symmetrisch, asymmetrisch, hybrid)

  - Hashwerte, Signaturen, Zertifikate

  - Authentifizierung (MFA)

  - Datenverfügbarkeit (Hinweis: RAID ist nicht mehr Bestandteil der Teil 1-Prüfung)

- Datenschutz (DSGVO, Datenschutzgesetz) ([Link: Datenschutz-Übungsaufgaben](https://oer-informatik.de/datenschutz-uebungsaufgaben))

#### 7. Erbringen der Leistungen und Auftragsabschluss

- Vertragsarten (Werk, Dienst, Service), Vertragsbestandteile und Vertragsstörungen

- Service Level Agreements (SLA)

- Zustandekommen eines Kaufvertrags und zugehörige Willenserklärungen

- Aufbauorganisation (Ein-/Mehrlinien, Matrix)

- Vollmachten, Prokura

- Unterschiede Kauf / Miete / Leasing

- Abnahme (Folgen, Aufbau eines Abnahmeprotokolls)

- Mängelarten (Schlechtleistung, Falschlieferung, Minderlieferung)

- Soll/Ist-Vergleiche, Lessons-Learned

- Veränderungsprozesse umsetzen

### Die gute Nachricht zum Schluss

Das klingt nach vielen Themen und einer unlösbaren Aufgabe! Jetzt aber die gute Nachricht: wer einigermaßen aufgepasst hat in der bisherigen Ausbildung, wer interessiert mit offenen Augen und Ohren durch den Betrieb und die Berufsschule läuft und wer gelernt hat, sich Notizen zu machen und zu organisieren, für den sind die Fragen erwartbar und die Fragentiefe auch ohne viel Hintergrundwissen beherrschbar.

Abgefragt wird alles ein bisschen, aber nichts wirklich tief - schließlich ist es die Prüfung aller IT-Berufe, von Anwendungsentwickler*innen über IT-Kaufleute zu IT-Systemelektroniker*innen. Keine Sorge - das ist mit etwas Vorbereitungsarbeit gut möglich. Aber fangt besser **jetzt** damit an!

Viel Erfolg!