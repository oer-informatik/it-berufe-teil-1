# Datenmengen berechnen

## Speicherbedarf Videostreaming

Ein Full-HD Videostream (1920 x 1080 Pixel) mit 30 Bildern pro Sekunde und einer Farbtiefe von 24 Bit/Pixel soll auf einer Festplatte unkomprimiert gespeichert werden. Der Videostream dauert 1,5h.

Berechnen Sie den erforderlichen Speicherplatz auf der Festplatte! Geben Sie das Ergebnis sowohl in GB als auch in GiB an!

<span class="hidden-text">

**Geg:**

Datenrate:
$d=30\frac{1}{s}$

Farbtiefe:
$ f = 24 \frac{Bit}{Pixel}$

Dauer:
$ t = 1,5 h = 90 min = 5400 s$

Anzahl Pixel:
$ Z = 1920 \cdot 1080 $

**Ges:** Speicherbedarf
$D_{gesamt}$

**Rechenweg:**
Anzahl Pixel:
$ Z = 1920 \cdot 1080  \text{ Pixel} = \text{2.073.600 Pixel} $
Speicherbedarf eines Bildes:
$D_{Bild} = Z \cdot f = \text{2.073.600} Pixel \cdot 24 \frac{Bit}{Pixel} = \text{49.766.400 Bit}$
Speicherbedarf pro Sekunde:
$D_{Sekunde} = D_{Bild} \cdot d = 49.766.400 Bit \cdot 30 \frac{1}{s} = 1492992000 \frac{Bit}{s}$

Speicherbedarf gesamt:
$D_{gesamt} = D_{Sekunde} \cdot t = 1.492.992.000 \frac{Bit}{s} \cdot 5400 s = 8.062.156.800.000 Bit$

$ = 8.062.156.800.000 Bit : 8 \frac{Bit}{Byte} = 1.007.769.600.000 Byte = 1.008 GB$

$1.007.769.600.000 Byte \cdot \frac{1 kiB}{1024 Byte} \cdot \frac{1 MiB}{1024 kiB} \cdot \frac{1 GiB}{1024 MiB} = 938 GiB$

oder direkte Umrechnung:

$1.008 \ GB \cdot \frac{1000^3}{1024^3} = 939 GiB $
</span>
