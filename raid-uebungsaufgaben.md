## RAID-Übungsaufgaben


<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/111914540092420470</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/raid-uebungsaufgaben</span>

> **tl/dr;** _(ca. 20 min Bearbeitungszeit): Übungsaufgaben mit Lösungen zum Thema RAID / Datenverfügbarkeit (z.B. zur Vorbereitung auf die [Teil-1-Prüfung der IT-Berufe](https://oer-informatik.de/fi-teil1-pruefung))..._


### Unterschied Software-RAID / Hardware-RAID

Nenne jeweils einen Vorteil und einen Nachteil für Software- und Hardware-RAIDs <button data-tabgroup="raidtxt" data-tabid="true" onclick="openTabsByDataAttr('true', 'raidtxt')">Antwort einblenden</button>

||Software-RAID|Hardware-RAID|
|---|---|---|
|Vorteile|<span class="tabs" data-tabgroup="raidtxt" data-tabid="true" style="display:none">günstig<br/>im Fehlerfall leicht wiederherzustellen</span>|<span class="tabs"  data-tabgroup="raidtxt" data-tabid="true" style="display:none">unabhängig von Serverhardware (z.B. durch gesonderte Pufferung bei Stromausfall)<br/> entlastet die Serverhardware (durch eigene Rechenleistung)</span>|
|Nachteile|<span class="tabs"  data-tabgroup="raidtxt" data-tabid="true" style="display:none">CPU des Servers wird belastet</span>|<span class="tabs" data-tabgroup="raidtxt" data-tabid="true" style="display:none">teuer</span>|

### Szenarien mit unterschiedlichen RAID -Varianten

Bewerten die RAID-Systeme RAID 1, RAID 0, RAID 0+1, RAID 10 , RAID 5, RAID 6 sowie ein JBOD mit jeweils 5 Laufwerken (à 5TB) hinsichtlich Ausfallwahrscheinlichkeit, Geschwindigkeit und Preis. Es langt, wenn Du Sie zueinander in Bezug setzt.

||Ausfallwahrscheinlichkeit|Geschwindigkeit|Preis pro TB<br/>relativ|Nutzdaten bei <br/>5x5TB|verlustfrei dürfen<br/> x Platten ausfallen
|---|---|---|---|---|---|
|RAID 0<br/><button data-tabgroup="raid0" data-tabid="true" onclick="openTabsByDataAttr('true', 'raid0')">show</button>|<span class="tabs" data-tabgroup="raid0" data-tabid="true" style="display:none">Fällt bereits bei einem Plattenschaden aus</span>|<span class="tabs" data-tabgroup="raid0" data-tabid="true" style="display:none">hoch</span>|<span class="tabs" data-tabgroup="raid0" data-tabid="true" style="display:none">günstig</span>|<span class="tabs" data-tabgroup="raid0" data-tabid="true" style="display:none">25 TB</span>|<span class="tabs" data-tabgroup="raid0" data-tabid="true" style="display:none">0</span>|
|RAID 1<br/><button data-tabgroup="raid1" data-tabid="true" onclick="openTabsByDataAttr('true', 'raid1')">show</button>|<span class="tabs" data-tabgroup="raid1" data-tabid="true" style="display:none">Fällt aus, sobald die letzte Platte ausfällt.</span>|<span class="tabs" data-tabgroup="raid1" data-tabid="true" style="display:none">nicht so schnell</span>|<span class="tabs" data-tabgroup="raid1" data-tabid="true" style="display:none">teuer</span>|<span class="tabs" data-tabgroup="raid1" data-tabid="true" style="display:none">5 TB</span>|<span class="tabs" data-tabgroup="raid1" data-tabid="true" style="display:none">4</span>|
|RAID 0+1<br/><button data-tabgroup="raid01" data-tabid="true" onclick="openTabsByDataAttr('true', 'raid01')">show</button>|<span class="tabs" data-tabgroup="raid01" data-tabid="true" style="display:none">alle redundanten Platten im selben Zweig (bei 4 Platten in jedem Fall 1, wenn es die richtigen trifft auch 2)</span>|<span class="tabs" data-tabgroup="raid01" data-tabid="true" style="display:none">hoch</span>|<span class="tabs" data-tabgroup="raid01" data-tabid="true" style="display:none">teuer</span>|<span class="tabs" data-tabgroup="raid01" data-tabid="true" style="display:none">10 TB</span>|<span class="tabs" data-tabgroup="raid01" data-tabid="true" style="display:none">1, ggf. 2</span>|
|RAID 10<br/><button data-tabgroup="raid10" data-tabid="true" onclick="openTabsByDataAttr('true', 'raid10')">show</button>|<span class="tabs" data-tabgroup="raid10" data-tabid="true" style="display:none">alle redundanten Platten (bei 4 Platten in jedem Fall 1, wenn es die richtigen trifft auch 2) </span>|<span class="tabs" data-tabgroup="raid10" data-tabid="true" style="display:none">hoch</span>|<span class="tabs" data-tabgroup="raid10" data-tabid="true" style="display:none">teuer</span>|<span class="tabs" data-tabgroup="raid10" data-tabid="true" style="display:none">10 TB</span>|<span class="tabs" data-tabgroup="raid10" data-tabid="true" style="display:none">1 ggf. 2</span>|
|RAID 5<br/><button data-tabgroup="raid5" data-tabid="true" onclick="openTabsByDataAttr('true', 'raid5')">show</button>|<span class="tabs" data-tabgroup="raid5" data-tabid="true" style="display:none">Sobald mehr als eine Platte ausfällt sind die Daten verloren.</span>|<span class="tabs" data-tabgroup="raid5" data-tabid="true" style="display:none">Lesegeschwindigkeit ähnlich zu RAID0, Schreibgeschwindigkeit etwas langsamer als Lesen wg. Paritätsberechnung </span>|<span class="tabs" data-tabgroup="raid5" data-tabid="true" style="display:none">billiger als RAID 1, 10, 0+1, teurer als RAID 0 </span>|<span class="tabs" data-tabgroup="raid5" data-tabid="true" style="display:none">20 TB</span>|<span class="tabs" data-tabgroup="raid5" data-tabid="true" style="display:none">1</span>|
|RAID 6<br/><button data-tabgroup="raid6" data-tabid="true" onclick="openTabsByDataAttr('true', 'raid6')">show</button>|<span class="tabs" data-tabgroup="raid6" data-tabid="true" style="display:none">höher als bei RAID 5, durch redundante Parität</span>|<span class="tabs" data-tabgroup="raid6" data-tabid="true" style="display:none">Wie Raid5: Lesegeschwindigkeit ähnlich zu RAID0, Schreibgeschwindigkeit etwas langsamer als Lesen wg. Paritätsberechnung </span>|<span class="tabs" data-tabgroup="raid6" data-tabid="true" style="display:none">teurer als RAID 5, billiger als 10 und 0+1</span>|<span class="tabs" data-tabgroup="raid6" data-tabid="true" style="display:none">15 TB</span>|<span class="tabs" data-tabgroup="raid6" data-tabid="true" style="display:none">2</span>|
|JBOD<br/><button data-tabgroup="jbod" data-tabid="true" onclick="openTabsByDataAttr('true', 'jbod')">show</button>|<span class="tabs" data-tabgroup="jbod" data-tabid="true" style="display:none">Mit jedem Laufwerksausfall sind alle Daten dieses Laufwerks verloren.</span>|<span class="tabs" data-tabgroup="jbod" data-tabid="true" style="display:none">ähnlich wie bei RAID1, da immer nur auf einer Platte geschrieben / gelesen wird.</span>|<span class="tabs" data-tabgroup="jbod" data-tabid="true" style="display:none">günstig</span>|<span class="tabs" data-tabgroup="jbod" data-tabid="true" style="display:none">25 TB</span>|<span class="tabs" data-tabgroup="jbod" data-tabid="true" style="display:none">0</span>|

### Verteilung der Datenpakete

Eine zusammenhängende Datei wird aufgeteilt in die Datenpakete A-E auf einem RAID aus 5 Laufwerke gespeichert. Wie werden die Datenpakete auf die Laufwerke verteilt? Verbinde gespiegelte Platten mit einer Linie mit der Beschriftung _mirror_, verbinde gekoppelte Platten mit einer Linie mit der Beschriftung _stripe_, Benenne Paritäten mit $P$ und streiche nicht genutzte Platten durch.

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID0](images/raid.png)

##### RAID0 <button onclick="toggleAnswer('raidpaket0')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket0">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID0](images/raid0.png)

</span>

##### RAID1 <button onclick="toggleAnswer('raidpaket1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket1">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID1](images/raid1.png)

</span>

##### RAID0+1 <button onclick="toggleAnswer('raidpaket01')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket01">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID01](images/raid01.png)

</span>

##### RAID10 <button onclick="toggleAnswer('raidpaket10')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket10">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID10](images/raid10.png)

</span>

##### RAID5 <button onclick="toggleAnswer('raidpaket5')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket5">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID5](images/raid5.png)

</span>

##### RAID6 <button onclick="toggleAnswer('raidpaket6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket6">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines RAID6](images/raid6.png)

</span>

##### JBOD <button onclick="toggleAnswer('raidpaketjbod')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaketjbod">

![Vorlage zum Verteilen der Datenpakete A-E auf 5 Platten eines JBOD](images/jbod.png)

</span>

### RAID 5 Paritätsverteilung

Ein RAID-5 ist bitweise XOR-Verknüpft. Welche Bit müssen in die fehlenden Stellen eingetragen werden?

![XOR-verknüpfte Bits eines RAID5 mit Lücken](images/raid5-bit.png)

<button onclick="toggleAnswer('raidpaket5bit')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="raidpaket5bit">

![XOR-verknüpfte Bits eines RAID5 - Lösung](images/raid5-bitlsg.png)

</span>
