## Datenschutz

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/111913950895667012</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/datenschutz-uebungsaufgaben</span>

> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Übungsaufgaben mit Lösungen zu Datenschutz, der DS-GVO und dem Datenschutzgesetz (z.B. zur Vorbereitung auf die [Teil-1-Prüfung der IT-Berufe](https://oer-informatik.de/fi-teil1-pruefung))..._

### Abgrenzung und Definition von Datenschutz und Datensicherheit

Grenze die Begriffe Datenschutz und Datensicherheit gegeneinander ab! 

**Datenschutz**: <button onclick="toggleAnswer('ds1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="ds1">

Datenschutz betrifft personenbezogene Daten. Festlegungen, wie mit Daten von identifizierten oder identifizierbaren Personen verfahren werden darf, werden in der [Datenschutz-Grundverordnung (DS-GVO)](https://www.bmj.de/DE/Themen/FokusThemen/DSGVO/_documents/Amtsblatt_EU_DSGVO.pdf;jsessionid=07CEA980A4E27B2C4B9313AF2A5C8EED.2_cid324?__blob=publicationFile&v=1) und dem [Bundesdatenschutzgesetz (BDSG)](https://www.gesetze-im-internet.de/bdsg_2018/inhalts_bersicht.html) festgelegt. Im Kern des Datenschutzes steht das Recht auf informationelle Selbstbestimmung von natürlichen Personen.

</span>

**Datensicherheit:** <button onclick="toggleAnswer('ds2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="ds2">

Unter dem Begriff "Datensicherheit" werden alle Maßnahmen zusammengefasst, die Unbefugte daran hindern, Daten zu erstellen, lesen, ändern oder löschen. Datensicherheit betrifft somit alle Daten, auch Daten ohne Personenbezug.

</span>

Handelt es sich hierbei um ein Datenschutz- oder ein Datensicherheitsproblem?

|Beschreibung|Datenschutz, Datensicherheit<br/> oder nichts von beidem?|
|:---|---|
|Ein Angebot, das die Kalkulationsgrundlagen ihres Unternehmens beinhaltet, liegt frei zugänglich im Internet| <button onclick="toggleAnswer('ds2a')">show</button><span class="hidden-answer" id="ds2a">Datensicherheit</span>|
|Die öffentlich einsehbare Telefonnummer des Supports wird von einem Unternehmen im internen Telefonbuch gespeichert.| <button onclick="toggleAnswer('ds2b')">show</button><span class="hidden-answer" id="ds2b">weder noch</span>|
|Eine Fitnessapp gibt die Daten über Sportaktivitäten einzelner Kunden an Krankenkassen für Werbezwecke weiter.| <button onclick="toggleAnswer('ds2c')">show</button><span class="hidden-answer" id="ds2c">Datenschutz</span>|
|Die Ergebnisse einer Internet-Umfrage Lieblingsreisezielen wird als Prozent-Angabe veröffentlicht| <button onclick="toggleAnswer('ds2d')">show</button><span class="hidden-answer" id="ds2d">weder noch</span>|
|Liste der Lieblingsreiseziele der anonymen Internet-Umfrage wird mit Reiseziel, Abstimmungszeit und IP-Adresse des abstimmenden veröffentlicht.| <button onclick="toggleAnswer('ds2e')">show</button><span class="hidden-answer" id="ds2e">Datenschutz</span>|
|Die Daten der Arbeitszeiterfassung werden durch ein Versehen mit fehlerhaften Daten überschrieben.| <button onclick="toggleAnswer('ds2f')">show</button><span class="hidden-answer" id="ds2f">Datensicherheit und Datenschutz</span>|
|Daten des Schießsystems mit persönlicher Keycard werden zum Abgleich der Arbeitszeiten verwendet.| <button onclick="toggleAnswer('ds2g')">show</button><span class="hidden-answer" id="ds2g">Datenschutz</span>|
|Artikeldaten aus einem Shopsystem können durch Dritte im Internet unbeabsichtigt verändert werden.| <button onclick="toggleAnswer('ds2h')">show</button><span class="hidden-answer" id="ds2h">Datensicherheit</span>|
|Bestelldaten eines Business-to-Business-Shops sind im Internet einsehbar.| <button onclick="toggleAnswer('ds2i')">show</button><span class="hidden-answer" id="ds2i">v.a. Datensicherheit</span>|
|Bestelldaten eines Endkunden-Shops sind im Internet einsehbar.| <button onclick="toggleAnswer('ds2j')">show</button><span class="hidden-answer" id="ds2j">Datenschutz und Datensicherheit</span>|
|Das Passwort des Rechners der Zeiterfassung klebt unter der Tastatur.| <button onclick="toggleAnswer('ds2k')">show</button><span class="hidden-answer" id="ds2k">Datenschutz und Datensicherheit</span>|
|Nach der Namensänderung hat ein Kunde keine Möglichkeit, seinen Namen in einem Shopsystem korrigieren zu lassen.| <button onclick="toggleAnswer('ds2l')">show</button><span class="hidden-answer" id="ds2l">Datenschutz</span>|

### Verbotsprinzip / Verbotsvorbehalt

Was besagt das Verbotsprinzip der Datenschutz-Grundverordnung? <button onclick="toggleAnswer('ds3')">Antwort ein/ausblenden</button>


<span class="hidden-answer" id="ds3">

Das Verbotsprinzip "besagt, dass grundsätzlich jede Verarbeitung personenbezogener Daten erst einmal verboten ist. Sie ist nur auf der Basis einer freiwilligen Entscheidung der betroffenen Person oder einer Rechtsgrundlage erlaubt.^[Der Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (Hrsg.): DSGVO – BDSG, Texte und Erläuterungen [(link)](https://www.bfdi.bund.de/SharedDocs/Downloads/DE/Broschueren/INFO1.pdf;jsessionid=CF186454F49E18DB7392D2AB6F0E4DB5.intranet241?__blob=publicationFile&v=9)]

</span>

### Rechte Betroffener

Über welche Rechte verfügen Betroffene gemäß der Datenschutz-Grundverordnung (DS-GVO)?

|Szenario|Ergibt sich das Recht aus der DS-GVO (ja, nein - Begründung)|
|:---|---|
|Betroffene natürliche Personen haben ein Recht auf Widerspruch der Nutzung der eigenen personenbezogenen Daten.|<button onclick="toggleAnswer('ds4a')">show</button><span class="hidden-answer" id="ds4a">ja, Widerspruchsrecht (Art. 21 DS-GVO)</span>|
|Natürliche Personen haben ein Recht auf Einschränkung der Verarbeitung der eigenen personenbezogenen Daten.|<button onclick="toggleAnswer('ds4b')">show</button><span class="hidden-answer" id="ds4b">ja, Einschränkung der Verarbeitung (Art. 18 DS-GVO)</span>|
|Körperschaften öffentlichen Rechts (Staat, Kommune,...) haben auf Basis der DS-GVO das Recht, auf veröffentlichte Unternehmensdaten (Bilanzen, Anteilseigner*innen, Vorstände, Prokurist*innen) zuzugreifen.|<button onclick="toggleAnswer('ds4c')">show</button><span class="hidden-answer" id="ds4c">nein</span>|
|Natürliche Personen haben das Recht gegenüber Körperschaften öffentlichen Rechts (Staat, Kommune,...), alle durch diese gespeicherten personenbezogenen Daten zu lesen, ändern oder löschen.|<button onclick="toggleAnswer('ds4d')">show</button><span class="hidden-answer" id="ds4d">nein</span>|
|Natürliche Personen haben ein Recht auf Auskunft zu eigenen personenbezogenen Daten.|<button onclick="toggleAnswer('ds4e')">show</button><span class="hidden-answer" id="ds4e">ja, Recht auf Auskunft zu personenbezogenen Daten (Art. 15 DS-GVO)</span>|
|Die Grundsätze der Datenschutzgrundverordnung verbieten es Whistleblowern in Deutschland, geheimgehaltene Daten zu Staaten, Organisationen und Unternehmen ohne deren Zustimmung zu veröffentlichen.|<button onclick="toggleAnswer('ds4f')">show</button><span class="hidden-answer" id="ds4f">Nein, die DS-GVO schützt nur personenbezogene Daten.</span>|
|Betroffene Firmen haben ein Recht auf Verschwiegenheit von Angestellten zu Unternehmensgeheimnissen aufgrund des Datenschutzes gemäß DS-GVO.|<button onclick="toggleAnswer('ds4g')">show</button><span class="hidden-answer" id="ds4g">Nein, die DS-GVO schützt nur personenbezogene Daten.</span>|
|Bei fehlerhaften personenbezogenen Daten haben betroffene natürliche Personen ein Recht auf Berichtigung.|<button onclick="toggleAnswer('ds4h')">show</button><span class="hidden-answer" id="ds4h">ja, Berichtigung (Art. 16 DS-GVO)</span>|
|Natürliche Personen haben ein Recht auf Weitergabe der eigenen personenbezogenen Daten (Datenübertragbarkeit).|<button onclick="toggleAnswer('ds4i')">show</button><span class="hidden-answer" id="ds4i">ja, Recht auf Datenübertragbarkeit (Art. 20 DS-GVO)</span>|
|Alle natürlichen Personen haben ein Recht auf Löschung der personenbezogenen Daten, wenn der Zweck der Erhebung entfallen ist.|<button onclick="toggleAnswer('ds4j')">show</button><span class="hidden-answer" id="ds4j">ja, Löschung (Art. 17 DS-GVO)</span>|
|Personenbezogenen Daten müssen auf Wunsch der betroffenen Person zu jedem Zeitpunkt durch den Erfasser/Verarbeiter gelöscht werden.|<button onclick="toggleAnswer('ds4k')">show</button><span class="hidden-answer" id="ds4k">Nein, nicht zu jedem Zeitpunkt: sie müssen erst gelöscht werden, wenn der Zweck entfällt. Zur Vertragserfüllung beispielsweise dürfen Sie verarbeitet werden.</span>|

Über welche Rechte verfügen betroffene Personen gemäß der Datenschutz-Grundverordnung (DS-GVO)? <button onclick="toggleAnswer('ds4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="ds4">

 - Informationspflicht und Recht auf Auskunft zu personenbezogenen Daten (Art. 15 DS-GVO)

 - Berichtigung (Art. 16 DS-GVO)
 
 - Löschung (Art. 17 DS-GVO)

 - Einschränkung der Verarbeitung (Art. 18 DS-GVO), 
 
 - Recht auf Datenübertragbarkeit (Art. 20 DS-GVO)

 - Widerspruchsrecht (Art. 21 DS-GVO)

</span>


### Grundsätze des Datenschutzes

Welche Grundsätze gemäß DS-GVO werden in den folgenden Absätzen beschrieben?

|Beschreibung|Grundsatz gemäß DS-GVO|
|:---|---|
|Personenbezogene Daten dürfen nur in einer für die betroffene Person nachvollziehbaren Art und Weise verarbeitet werden.| <button onclick="toggleAnswer('ds5a')">show</button><span class="hidden-answer" id="ds5a"> Transparenz (DS-GVO Art. 5 1a)</span>|
|Es muss im Vorhinein veröffentlicht werden, wofür die personenbezogene Daten erhoben werden. Sie dürfen im Folgenden davon abweichend nicht verwendet werden.| <button onclick="toggleAnswer('ds5b')">show</button><span class="hidden-answer" id="ds5b">Zweckbindung (DS-GVO Art. 5 1b)</span>|
|Es dürfen nur so viele Daten erfasst werden, wie es für den jeweiligen Zweck erforderlich ist.| <button onclick="toggleAnswer('ds5c')">show</button><span class="hidden-answer" id="ds5c">Datenminimierung (DS-GVO Art. 5 1c)</span>|
|Es muss dafür gesorgt werden, dass die gespeicherten personenbezogenen Daten aktuell sind und stimmen - andernfalls müssen sie korrigiert oder gelöscht werden.| <button onclick="toggleAnswer('ds5d')">show</button><span class="hidden-answer" id="ds5d">Richtigkeit (DS-GVO Art. 5 1d)</span>|
|Der Personenbezug der Daten darf nur so lange bestehen, wie es zur Erfüllung des Zwecks erforderlich ist.| <button onclick="toggleAnswer('ds5e')">show</button><span class="hidden-answer" id="ds5e">Speicherbegrenzung (DS-GVO Art. 5 1e)</span>|
|Mithilfe technisch-organisatorischer Maßnahmen muss sichergestellt werden, dass Dritte die Daten nicht sehen, löschen, ändern können.| <button onclick="toggleAnswer('ds5f')">show</button><span class="hidden-answer" id="ds5f">Vertraulichkeit und Integrität (DS-GVO Art. 5 1f)</span>|
|Die Speicherung von Daten unterliegt einer zeitlichen Begrenzung.| <button onclick="toggleAnswer('ds5g')">show</button><span class="hidden-answer" id="ds5g">Speicherbegrenzung (DS-GVO Art. 5 1e)</span>|
|Die verantwortliche Stelle muss jederzeit umfassende Informationen an die betroffenen Personen geben können, welche Daten durch wen und zu welchen Zwecken verarbeitet werden und wurden.| <button onclick="toggleAnswer('ds5h')">show</button><span class="hidden-answer" id="ds5h"> Transparenz (DS-GVO Art. 5 1a)</span>|
|Die Richtigkeit der Datenverarbeitung muss gewährleistet sind und es besteht ein Aktualisierungsanspruch bei Fehlern.| <button onclick="toggleAnswer('ds5i')">show</button><span class="hidden-answer" id="ds5i">Richtigkeit (DS-GVO Art. 5 1d)</span>|
|Datenerhebung muss dem Zweck angemessen und auf das notwendige Maß beschränkt sein.| <button onclick="toggleAnswer('ds5j')">show</button><span class="hidden-answer" id="ds5j">Datenminimierung (DS-GVO Art. 5 1c)</span>|
|Der Schutz personenbezogener Daten vor unerlaubtem Zugriff und Veränderung muss durch technische und organisatorische Maßnahmen sichergestellt sein – Datenschutz durch Technik, datenschutzfreundliche Voreinstellungen, Zertifizierungsverfahren und Datenschutzsiegel.| <button onclick="toggleAnswer('ds5k')">show</button><span class="hidden-answer" id="ds5k">Vertraulichkeit und Integrität (DS-GVO Art. 5 1f)</span>|
|Die Zwecke der Datenverarbeitung müssen bereits bei der Erhebung festgelegt, eindeutig und legitim sein.| <button onclick="toggleAnswer('ds5l')">show</button><span class="hidden-answer" id="ds5l">Zweckbindung (DS-GVO Art. 5 1b)</span>|

### Technisch-Organisatorische Maßnahmen

Welche **Technisch-Organisatorischen Maßnahmen (TOM)** werden in der DS-GVO definiert? <button onclick="toggleAnswer('ds6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="ds6">

In [Artikel 32 DS-GVO](https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679#d1e3395-1-1):

- Pseudonymisierung personenbezogener Daten (Art. 32 1a DS-GVO)

- Verschlüsselung personenbezogener Daten (Art. 32 1a DS-GVO)

- die Fähigkeit, die Vertraulichkeit, Integrität, Verfügbarkeit und Belastbarkeit der Systeme und Dienste im Zusammenhang mit der Verarbeitung auf Dauer sicherzustellen; (Art. 32 1b DS-GVO).

- die Fähigkeit, die Verfügbarkeit der personenbezogenen Daten und den Zugang zu ihnen bei einem physischen oder technischen Zwischenfall rasch wiederherzustellen; (Art. 32 1c DS-GVO)

- ein Verfahren zur regelmäßigen Überprüfung, Bewertung und Evaluierung der Wirksamkeit der technischen und organisatorischen Maßnahmen zur Gewährleistung der Sicherheit der Verarbeitung. (Art. 32 1d DS-GVO)

</span>

### Bewertung eines Szenarios

Sie bitten bei einem Unternehmen per E-Mail um ein Angebot für eine Urlaubsreise. Was ist zulässig?

Notieren Sie sich über die Entscheidung der Zulässigkeit hinaus noch die jeweilige Begründung!

|Szenario|zulässig gemäß DS-GVO (Begründung)?|
|:---|---|
|Die Firma darf ein Angebot an ihre E-Mail-Adresse schicken.| <button onclick="toggleAnswer('ds7a')">show</button><span class="hidden-answer" id="ds7a">Ja, diese Datennutzung ist zur Erfüllung vorvertraglicher Maßnahmen auf Anfrage hin zulässig.</span>|
|Die Firma ihren Namen und die Mail-Adresse speichern.| <button onclick="toggleAnswer('ds7b')">show</button><span class="hidden-answer" id="ds7b">Ja, diese Datennutzung ist zur Erfüllung vorvertraglicher Maßnahmen auf Anfrage hin zulässig.</span>|
|Die Firma darf ihre Postadresse und das Instagramm-Profilbild recherchieren und in einer Datenbank mit ihren Kundendaten speichern.| <button onclick="toggleAnswer('ds7c')">show</button><span class="hidden-answer" id="ds7c">Nein, diese Datennutzung geht über den vereinbarten Zweck hinaus und verstößt z.B. gegen Zweckbindung und Datenminimierung.</span>|


### Links und weitere Informationen

- [Der Bundesbeauftragte für den Datenschutz und die Informationsfreiheit (Hrsg.): DSGVO – BDSG, Texte und Erläuterungen](https://www.bfdi.bund.de/SharedDocs/Downloads/DE/Broschueren/INFO1.pdf;jsessionid=CF186454F49E18DB7392D2AB6F0E4DB5.intranet241?__blob=publicationFile&v=9)

- [Datenschutz-Grundverordnung (DS-GVO) im Volltext](https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016R0679)

- [Bundesdatenschutzgesetz (BDSG) im Volltext](https://www.gesetze-im-internet.de/bdsg_2018/index.html)
