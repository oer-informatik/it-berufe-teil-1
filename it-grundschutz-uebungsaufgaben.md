## IT-Grundschutz


<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/111913972099772424</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/it-grundschutz-uebungsaufgaben</span>

> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Übungsaufgaben mit Lösungen zum IT-Grundschutz nach BSI (z.B. zur Vorbereitung auf die [Teil-1-Prüfung der IT-Berufe](https://oer-informatik.de/fi-teil1-pruefung))..._



### Schutzziele nennen (nach BSI)

Nenne die drei Schutzziele, die der IT-Grundschutz nach BSI kennt:<button onclick="toggleAnswer('itgs1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="itgs1">

Das BSI definiert die Schutzziele Integrität, Vertraulichkeit und Verfügbarkeit.

</span>

### Schutzziele beschreiben (nach BSI)

Beschreibe, was unter den folgenden Schutzzielen verstanden wird:

**Integrität** <button onclick="toggleAnswer('itgs2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="itgs2">

Die Korrektheit, Unversehrtheit und Vollständigkeit der Informationen und der Funktionsweise von Systemen ist gegeben ([BSI Lerneinheit 4.1](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/Zertifizierte-Informationssicherheit/IT-Grundschutzschulung/Online-Kurs-IT-Grundschutz/Lektion_4_Schutzbedarfsfeststellung/4_01_Definitionen.html))

</span>


**Vertraulichkeit** <button onclick="toggleAnswer('itgs3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="itgs3">

Vertrauliche Informationen dürfen nicht unberechtigt zur Kenntnis genommen oder weitergegeben werden. ([BSI Lerneinheit 4.1](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/Zertifizierte-Informationssicherheit/IT-Grundschutzschulung/Online-Kurs-IT-Grundschutz/Lektion_4_Schutzbedarfsfeststellung/4_01_Definitionen.html))

</span>

**Verfügbarkeit** <button onclick="toggleAnswer('itgs4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="itgs4">

Autorisierte Benutzer sollen jederzeit ungehinderten Zugriff auf Informationen und Systeme haben. ([BSI Lerneinheit 4.1](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/Zertifizierte-Informationssicherheit/IT-Grundschutzschulung/Online-Kurs-IT-Grundschutz/Lektion_4_Schutzbedarfsfeststellung/4_01_Definitionen.html))

</span>


### Einordnung der Schutzziele

Welches der Schutzziele wird durch die beschriebenen Maßnahmen erreicht?

|Beschreibung|Vertraulichkeit|Integrität|Verfügbarkeit|
|:---|---|---|---|
|E-Mails werden künftig mit TLS verschlüsselt versendet. <button onclick="toggleAnswer('itgs6a')">show</button>|<span class="hidden-answer" id="itgs6a">X</span>|||
|Zur Datenspeicherung wird ein RAID5 eingesetzt. <button onclick="toggleAnswer('itgs6b')">show</button>|||<span class="hidden-answer" id="itgs6b">X</span>|
|Wichtige Formulare werden nur noch als PDF herausgegeben und in einem Dokumentenmanagementsystem versioniert.<button onclick="toggleAnswer('itgs6c')">show</button>||<span class="hidden-answer" id="itgs6c">X</span>||
|Nachrichten werden mithilfe von PGP signiert.<button onclick="toggleAnswer('itgs6d')">show</button>||<span class="hidden-answer" id="itgs6d">X</span>||
|Der Zugriff auf Internetseiten des Unternehmens erfolgt nur noch über HTTPS und nicht mehr über HTTP.<button onclick="toggleAnswer('itgs6e')">show</button>|<span class="hidden-answer" id="itgs6e">X</span>|||
|Es wird eine Übung durchgeführt, innerhalb welcher Zeit die Daten aus einem Backup wiederhergestellt werden können.<button onclick="toggleAnswer('itgs6f')">show</button>|||<span class="hidden-answer" id="itgs6f">X</span>|


### Schutzbedarfsanalyse

Ordne für jedem der drei Schutzziele einen Schutzbedarf für das genannte IT-System zu und Begründe, warum!

Schutzbedarfseinordnung der Geschäftsprozesse: 

|Schutzbedarf|Auswirkungen auf den Geschäftsprozess|
|:---|:---|
|niedrig-mittel|kaum Auswirkungen auf den Geschäftsprozess|
|hoch|GP nur noch mit deutlichem Aufwand durchführbar|
|sehr hoch|GP überhaupt nicht mehr durchführbar oder existenzielle Probleme|

|IT-System Beschreibung|Schutzziel|Schutzbedarf|Begründung|
|:---|---|---|---|
|Zeiterfassungssystem der Angestellten|Vertraulichkeit<br/>Integrität<br/>Verfügbarkeit|||
|Rechner in der HR-Abteilung<br/>mit Bewerbungsdaten und Krankmeldungen|Vertraulichkeit<br/>Integrität<br/>Verfügbarkeit|||
|Alarmanlage für Brandmeldungen|Vertraulichkeit<br/>Integrität<br/>Verfügbarkeit|||

### Schutzziele

Welche der folgenden Aussagen zu Schutzzielen ist korrekt?

|Aussage|korrekt (ja/nein), Begründung|
|:---|:---|
|Autorisierung beschreibt die Eigenschaft, dass Daten sind unversehrt, richtig und vollständig sind.<button onclick="toggleAnswer('itgs8c')">show</button>|<span class="hidden-answer" id="itgs8c">Nein, Autorisierung ist kein Schutzziel im Sinne des BSI und beschreibt den Vorgang, dass jemandem Rechte eingeräumt werden, bestimmte Daten zu verwenden, lesen, schreiben, ändern, löschen.</span>|
|Das Schutzziel Verschlüsselung beschreibt den Vorgang, sich mit einem _public key_ gegenüber einem Server zu autorisieren. <button onclick="toggleAnswer('itgs8a')">show</button>|<span class="hidden-answer" id="itgs8a">Nein, Autorisierung ist nicht Bestandteil des Schutzziels Verschlüsselung. Der geschilderte Vorgang beschreibt am ehesten eine Authentifizierung, diese ergibt mit einem _public key_ ohne weitere Vorgaben jedoch auch keinen Sinn.</span>|
|Das Schutzziel Integrität beschreibt, dass Daten unversehrt, richtig und vollständig sind.<button onclick="toggleAnswer('itgs8b')">show</button>|<span class="hidden-answer" id="itgs8b">Ja, genau das beschreibt Integrität.</span>|
|Unter Authentizität versteht man, dass Daten von der angegebenen Quelle stammen.<button onclick="toggleAnswer('itgs8d')">show</button>|<span class="hidden-answer" id="itgs8d">Ja, Authentizität beschreibt, dass jemand ist, wer er/sie vorgibt zu sein. Die Prüfung der Authentizität sollte also der Autorisierung voran gehen. Authentizität ist aber kein BSI Schutzziel.</span>|
|Integrität von Daten beschreibt die Tatsache, dass Daten von der Quelle stammen, von der sie vorgeben zu stammen.<button onclick="toggleAnswer('itgs8e')">show</button>|<span class="hidden-answer" id="itgs8e">Nein, das beschriebene ist Authentizität.</span>|
|Vertraulichkeit beschreibt, dass Daten von unbefugten nicht gelesen oder verwendet werden können.<button onclick="toggleAnswer('itgs8f')">show</button>|<span class="hidden-answer" id="itgs8f">Ja.</span>|
